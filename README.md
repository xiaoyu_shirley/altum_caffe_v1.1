### SSD

This caffe version is based on the original [ssd](https://github.com/weiliu89/caffe/tree/ssd) with extra support for DepthWiseConvLayer, ROIPoolingLayer and updated SmoothL1LossLayer from [fasterRcnn](https://github.com/rbgirshick/py-faster-rcnn).

### MobileNet-SSD 

**Only if you want to run the demo**

- Download this [repo](https://github.com/chuanqi305/MobileNet-SSD) under this caffe folder.
- Generate deploy file according to the tutorial based on your model.
- Modify demo.py accordingly.

### Faster RCNN

In order to run demo from faster rcnn, you still need to add some folders from [FRCNN](https://github.com/rbgirshick/py-faster-rcnn), especially the `lib` folder. Because some functions required by FRCNN's demo are implemented using Python scripts.
